

Install dependencies

```bash
yarn install
```

Set environment variables

```bash
echo 'VITE_API_URL' >> .env
```

Run server

```bash
yarn dev
```
