import { ReactLocation, createBrowserHistory, Route, DefaultGenerics } from "react-location";
import MainLayout from './layouts/MainLayout'
import Home from './pages/Home'


export const location = new ReactLocation({
    history: createBrowserHistory()
})

export const routes: Route<DefaultGenerics>[] = [
    {
        path: '/',
        element: <MainLayout><Home/></MainLayout>
      },
      {
        path: '/clients',
        element: () => import('./pages/clients/index')
           .then(module => (<MainLayout><module.default /></MainLayout> ))
      },
      {
        path: '/delivers',
        element: () => import('./pages/delivers/index')
           .then(module => (<MainLayout><module.default /></MainLayout> ))
      },
      {
        path: '/parcels',
        element: () => import('./pages/parcels/index')
           .then(module => (<MainLayout><module.default /></MainLayout> ))
      },
      {
        path: '/requests',
        children: [
          {
            path: '/',
            element: () => import('./pages/requests/index')
            .then(module => (<MainLayout><module.default /></MainLayout> )),
          },
          {
            path: '/create',
            element: () => import('./pages/requests/create')
            .then(module => (<MainLayout><module.default /></MainLayout> )),
          },
          {
            path: '/:requestId',
            loader: async ({ params: { requestId } }) => {
              return {
                requestId
              }
            },
            element: () => import('./pages/requests/detail')
            .then(module => (<MainLayout><module.default /></MainLayout> )),
          }
        ]
      },
      {
        path: '*',
        element: () => import('./pages/error/404')
           .then(module => (<module.default />))
      }
]