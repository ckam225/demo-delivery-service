import { Address } from "../types/types";


export function formatAddress(address:Address){
    const l=Object.values(address);
    return l.slice(1, l.length - 1).join(', ')
}