import * as React from 'react';
import DeliverGird from "../../components/delivers/DeliverGrid";
import { Deliver } from "../../types/types";
import { fetchDelivers } from "../../api/delivers";
import Modal, { ModalForwardProps } from '../../components/Modal';
import DeliveryDetail from '../../components/delivers/DeliverDetail';




const DeliverPage:React.FC = () => {

    const [delivers, setDelivers] = React.useState<Deliver[]>([])
    const [currentDeliver, setCurrentDeliver] = React.useState<Deliver>()
    const modalRef = React.useRef<ModalForwardProps>()

    React.useEffect(() => {
        setTimeout(async () => {
            const res = await fetchDelivers()
            setDelivers(res.data as Deliver[])
        }, 0)
    }, [])

    function handleItemClick(deliver:Deliver){
      setCurrentDeliver(deliver)
      modalRef.current?.open()
    }

    return <React.Fragment>
      <div className="p-5 w-full">
        <div className="flex justify-between items-center">
          <div className="flex items-center py-2">
            <h1>Компании доставки</h1>
          </div>
          <div className="flex">
            
          </div>
        </div>
        <DeliverGird delivers={delivers} onItemClick={handleItemClick}/>
     </div>

    <Modal refId={modalRef}>
      <Modal.Header>
         {currentDeliver?.name}
      </Modal.Header>
      <Modal.Body>
          {currentDeliver && <DeliveryDetail deliver={currentDeliver}/>}
      </Modal.Body>
    </Modal>
</React.Fragment>

}
 
export default DeliverPage;