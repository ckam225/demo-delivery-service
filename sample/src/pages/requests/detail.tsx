
import * as React from 'react'
import GoodTable from "../../components/goods/GoodTable";
import QuoteDeliveryCostTable from "../../components/requests/QuoteDeliveryCostTable";
import { VscArrowLeft } from "react-icons/vsc";
import { Link, MakeGenerics, useMatch } from 'react-location';
import {  QuoteDeliveryCost, Request } from '../../types/types';
import { fetchRequest, proposeDeliveryCost } from '../../api/requests';
import Modal, { ModalForwardProps } from '../../components/Modal';
import ProposeRequestCostForm from '../../components/requests/ProposeRequestCostForm';
import { useAddress } from '../../hooks/useAddress';
import ParcelForm from '../../components/parcels/ParcelForm';
import { createParcel } from '../../api/parcels';
import { ParcelPayload } from '../../types/payloads';

type LocationGenerics = MakeGenerics<{
    Params: {
        requestId:string
    };
}>;



const RequestDetailPage: React.FC = () => {
    const [request, setRequest] = React.useState<Request>({} as Request)
    const [chosenQuote, setChosenQuote] = React.useState<QuoteDeliveryCost>({} as QuoteDeliveryCost)
    const [isSubmittingProposal, setIsSubmittingProposal] = React.useState<boolean>(false)
    const params = useMatch<LocationGenerics>().params
    const modalRef = React.useRef<ModalForwardProps>()
    const modalQuoteRef = React.useRef<ModalForwardProps>()
    const {dispatchAddress, deliveryAddress} = useAddress(request)
    const [parcelSubmitting, setParcelSubmitting] = React.useState<boolean>(false)

    async function handleSubmitProposalCost(value: any){
        try{
            setIsSubmittingProposal(true)
            const res = await proposeDeliveryCost({...value, request_id: request.id})
            loadRequest()
            modalRef.current?.close()
    
        }catch(err){
            console.error(err);
        }finally{
            setIsSubmittingProposal(false)
        }
    }

    async function loadRequest(){
        if(params.requestId){
            const res= await fetchRequest(params.requestId)
            setRequest(res.data as Request)
        }
    }

    async function handleChoseDeliveryCost(quote: QuoteDeliveryCost){
        setChosenQuote(quote)
        modalQuoteRef.current?.open()
        console.log('chosen quote', quote);
        
    }

    function handleParcelReset(){
        modalQuoteRef.current?.close()
    }

    async function handleParcelSubmit(parcel: ParcelPayload){
        try {
            console.log('parcel from', parcel);
            
            const res = await createParcel(parcel)
            console.log(res);
            modalQuoteRef.current?.close()  
        } catch (err) {
            console.log(err);
        }
    }

    React.useEffect(() => {
        loadRequest()
    }, [params.requestId])

  
    return <> 
        {request ? <>
            <div className="w-full">
            <div className="p-5 flex justify-between items-center">
                <div className="flex">
                <Link to="/requests" className="flex items-center text-primary-dark m-2">
                    <VscArrowLeft/>
                    Назад
                </Link>
                </div>
            </div>
            <div className="w-full flex justify-between">
                <div className="p-5">
                    <div className="py-1">
                        <div className="text-gray-500">Уникальный индентификатор: </div> 
                        <em>{request.id}</em>
                    </div>
                    <div className="py-1">
                        <div className="text-gray-500">Клиент: </div> 
                        <em>{request.client?.name}</em>
                    </div>
                    <div className="py-1">
                    <div className="text-gray-500">Тип доставки: </div> 
                    <em>{request.delivery_type?.name}</em>
                    <p className="text-xs">{request.delivery_type?.description}</p>
                    </div>
                </div>
                <div className="p-5">
                    <div className="py-1">
                        <div className="text-gray-500">Адрес отправления</div>
                        <div className=" text-sm">
                            {dispatchAddress}
                        </div>
                    </div>
                    <div className="py-1">
                        <div className="text-gray-500">Адрес получения</div>
                        <div className=" text-sm">
                            {deliveryAddress}
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex p-5 justify-between">
            <div className="text-primary-dark">Список грузов</div>
            {/* <button className="btn">Добавить товар</button> */}
            </div>
            
            <div className="flex w-full p-5">
                {request?.goods && <GoodTable goods={request.goods}/>}
            </div>

            <div className="flex p-5 justify-between">
            <div className=" text-primary-dark">Стоимости по компании</div>
            <button className="btn" onClick={() => modalRef.current?.open()}>Предложить стоимость доставки</button>
            </div>
            <div className="p-5">
                <QuoteDeliveryCostTable quotes={request?.quote_delivery_costs || []} onChoose={handleChoseDeliveryCost}/>
            </div>
        </div>

        <Modal refId={modalRef}>
            <Modal.Header>
               Предложить стоимость доставки
            </Modal.Header>
            <Modal.Body>
               <ProposeRequestCostForm 
                  handleSubmit={handleSubmitProposalCost} 
                  isSubmitting={isSubmittingProposal}/>
            </Modal.Body>
        </Modal>

        <Modal refId={modalQuoteRef}>
            <Modal.Header>
               Создать Посылку
            </Modal.Header>
            <Modal.Body>
               <ParcelForm 
                  quote={chosenQuote} 
                  request={request} 
                  onReset={handleParcelReset}
                  onSubmit={handleParcelSubmit}
                  submitting={parcelSubmitting}
                />
            </Modal.Body>
        </Modal>
        </> : <div>No request found</div>}
    </>
}
 
export default RequestDetailPage;