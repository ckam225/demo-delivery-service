
import * as React from 'react';
import { Link } from 'react-location';
import { fetchRequests } from '../../api/requests';
import RequestTable from "../../components/requests/RequestTable";
import { Request } from "../../types/types";


const RequestHomePage:React.FC = () => {

    const [requests, setRequests] = React.useState<Request[]>([])

    React.useEffect(() => {
        setTimeout(async () => {
            const res = await fetchRequests()
            setRequests(res.data as Request[])
        }, 0)
    }, [])

    return <div className="p-5 w-full">
       <div className="flex justify-between items-center">
         <h1>Запросы доставок</h1>
         <div className="flex">
            <Link to="/requests/create" className="link-button m-2">
               Create request
            </Link>
         </div>
       </div>
        <RequestTable requests={requests}/>
    </div>
}

export default RequestHomePage;