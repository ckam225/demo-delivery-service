import { VscArrowLeft } from "react-icons/vsc";
import AddressForm from "../../components/address/AddressForm";
import GoodListForm from "../../components/goods/GoodListForm";
import { fetchClients } from "../../api/clients";
import { fetchDeliveryTypes } from "../../api/deliveryTypes";
import { createEagalRequests } from "../../api/requests";
import { Link } from "react-location";
import { Address, Client, DeliveryType, Good } from "../../types/types";
import React, { FormEvent, useState } from "react";


type RequestForm = {
    dispatch_address?: Address
    delivery_address?: Address
    client_id: string
    delivery_type_id: string
    goods: Good[]
}

const CreateRequestPage = ({}) => {
    const [clients, setClients] = useState<Client[]>([])
    const [deliveryTypes, setDeliveryTypes] = useState<Client[]>([])
    const [loading, setLoading] = useState(false)

    const [values, setValues] = useState<RequestForm>({
        client_id: '',
        delivery_type_id: '',
        goods: []
    })

    React.useEffect(() => {
        loadClients()
        loadDeliveryTypes()
    }, [])

    async function loadClients(){
        setTimeout(async () => {
            const res = await fetchClients()
            setClients(res.data as Client[])
        }, 0)
    }

    async function loadDeliveryTypes(){
        setTimeout(async () => {
            const res = await fetchDeliveryTypes()
            setDeliveryTypes(res.data as DeliveryType[])
        }, 0)
    }

    function handleFieldChange(e:React.ChangeEvent<HTMLInputElement|HTMLSelectElement>){
        const newFormValue = {...values} as any
        newFormValue[e.target.name] = e.target.value
       setValues(newFormValue)
    }

    function handleGoodsChange(goods:Good[]){
        const newFormValue = {...values} as RequestForm
        newFormValue.goods = goods
        setValues(newFormValue)
    }

    function handleDispatchAddressChange(address:Address){
        const newFormValue = {...values} as RequestForm
        newFormValue.dispatch_address = address
        setValues(newFormValue)
    }

    function handleDeliveryAddressChange(address:Address){
        const newFormValue = {...values} as RequestForm
        newFormValue.delivery_address = address
        setValues(newFormValue)
    }

    async function handleSubmit(e:FormEvent){
        e.preventDefault()
        try {
        //if(values.client_id && values.delivery_type_id && values.goods.length && values.dispatch_address && values.delivery_address){

            setLoading(true)
            const req =  await createEagalRequests(values)
        //}
        } catch (error) {
            console.log(error);
        }
        finally {
            setLoading(false)
        }
    }

    return <form className="p-5 w-full" onSubmit={handleSubmit}>
        <div className="flex justify-between items-center">
           <Link to="/requests" className="flex items-center text-primary-dark m-2">
                        <VscArrowLeft/>
                        Назад
                </Link>
            <div className="flex flex-1 justify-around">
                <h1 className="ml-4">Создать запросы доставок</h1>
            </div>
        </div>
        
        <div className="flex w-full flex-col p-5">
            <h1>Адрес отправления</h1>
            <AddressForm className="flex-1" inputClass="my-2" handleChange={handleDispatchAddressChange}/>
            <h1 className="mt-5">Адрес получения</h1>
            <AddressForm  className="flex-1" handleChange={handleDeliveryAddressChange}/>
        </div>

        <div className="w-full p-5 flex">
            <div className="w-full">
                <div>Клиент</div>
                <select name="client_id" className="field" value={values.client_id}  onChange={handleFieldChange}>
                  <option value=""></option>
                {clients.map(client => <option value={client.id} key={client.id}>{client.name}</option>)}
                </select>
            </div>
            <div className="w-full ml-5">
                <div>Тип доставки</div>
                <select name="delivery_type_id" className="field" value={values.delivery_type_id} onChange={handleFieldChange}>
                    <option value=""></option>
                    {deliveryTypes.map(dt => <option value={dt.id} key={dt.id}>{dt.name}</option>)}
                </select>
            </div>
        </div>

        <div className="flex w-full flex-col p-5">
            <h1>Список грузов</h1>
            <GoodListForm handleChange={handleGoodsChange}/>
        </div>

        {!loading && <div className="flex justify-end items-center p-5">
            <button className="btn btn-danger mr-2" type="reset">Отменить</button>
            <button className="btn" type="submit">Отправить</button>
        </div>}

 </form>
}

export async function getServerSideProps(){

    const clients = await fetchClients();
    const deliveryTypes = await fetchDeliveryTypes()

    return {
        props: {
            clients: clients.data || [],
            deliveryTypes: deliveryTypes.data || []
        }
    }
}
 
export default CreateRequestPage;