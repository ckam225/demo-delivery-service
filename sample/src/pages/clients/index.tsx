import { useEffect, useState } from "react";
import { fetchClients } from "../../api/clients";
import ClientTable from "../../components/clients/ClientTable";
import { Client } from "../../types/types";

const ClientHomePage = () => {

   const [clients, setClients] = useState<Client[]>([])

   useEffect(() => {
     (async ()=> {
       const res = await fetchClients()
       setClients(res.data || [])
     })()
   }, [])

    return <div className="p-5 w-full">
       <div className="flex justify-between items-center">
         <h1>Клиенты</h1>
         <div className="flex">
            
         </div>
       </div>
        <ClientTable clients={clients}/>
    </div>
}


export async function getServerSideProps() {
    const res = await fetchClients()
    return {
      props: {
        clients: res.data || []
      }
    }
  }
 

export default ClientHomePage;