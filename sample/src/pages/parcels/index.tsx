import { useEffect, useState } from "react";
import { fetchParcels } from "../../api/parcels";
import ParcelTable from "../../components/parcels/ParcelTable";
import { Parcel } from "../../types/types";

const ParcelHomePage = () => {

   const [parcels, setParcels] = useState<Parcel[]>([])

   useEffect(() => {
     (async ()=> {
       const res = await fetchParcels()
       setParcels(res.data || [])
     })()
   }, [])

   function handleItemClick(parcel:Parcel){
     console.log(parcel);
     
   }

    return <div className="p-5 w-full">
       <div className="flex justify-between items-center">
         <h1>Клиенты</h1>
         <div className="flex">
            
         </div>
       </div>
        <ParcelTable parcels={parcels} onItemClick={handleItemClick}/>
    </div>
}


export async function getServerSideProps() {
    const res = await fetchParcels()
    return {
      props: {
        parcels: res.data || []
      }
    }
}
 

export default ParcelHomePage;