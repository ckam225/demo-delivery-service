import * as React from 'react';
import { formatAddress } from '../helpers/formatter';



export function useAddress(obj:any){

    const dispatchAddress = React.useMemo(() => {
        return formatAddress(obj?.dispatch_address || {})
    }, [obj])

    const deliveryAddress = React.useMemo(() => {
        return formatAddress(obj?.delivery_address || {})
    }, [obj])

    return {
        dispatchAddress,
        deliveryAddress
    }
}