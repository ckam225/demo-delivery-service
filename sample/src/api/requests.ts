
import {  BaseHttpResponse, Request } from "../types/types"
import api from "./axios"

interface RequestResponse extends BaseHttpResponse {
   data: Request | Request[] 
}

export async function fetchRequests(): Promise<RequestResponse>{
    const req = await api.get(`/requests`)
    const data =  {
        status: req.status,
        error: req.statusText,
        data: (req.data?.data || []) as Request[]
    } as RequestResponse
    return data
}

export async function fetchRequest(id:string): Promise<RequestResponse>{
    const req = await api.get(`/requests/${id}`)
    const data =  {
        status: req.status,
        error: req.statusText,
        data: (req.data?.data) as Request
    } as RequestResponse
    return data
}

export async function createRequests(payload:any){
    const req = await api.post(`/requests/`, payload)
    return req.data
}

export async function createEagalRequests(payload:any){
    const req = await api.post(`/requests/eagle-store`, payload)
    return req.data
}

export async function updateRequests(id:string, payload:any){
    const req = await api.put(`/requests/${id}`, payload,{
        headers: {
            "Content-Type": "application/json",
        }
    })
    return req.data 
}

export async function proposeDeliveryCost(payload:any){
    const req = await api.post(`/requests/propose-delivery-costs`, payload)
    return req.data
}

