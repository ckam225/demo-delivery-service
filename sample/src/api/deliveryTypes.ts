
import api from './axios'


export async function fetchDeliveryTypes(){
    const req = await api.get(`/delivery_types`)
    return req.data
}

export async function fetchDeliveryType(id:string){
    const req = await api.get(`/delivery_types/${id}`)
    return req.data
}

