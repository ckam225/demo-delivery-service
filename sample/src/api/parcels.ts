
import { ParcelPayload } from '../types/payloads'
import api from './axios'

export async function fetchParcels(){
    const req = await api.get('/parcels')
    return req.data
}

export async function fetchParcel(id:string){
    const req = await api.get(`/parcels/${id}`)
    return req.data
}

export async function createParcel(payload: ParcelPayload){
    const req = await api.post(`/parcels`, payload)
    return req.data
}


