
import api from './axios'

export async function fetchClients(){
    const req = await api.get('/clients')
    return req.data
}

export async function fetchClient(id:string){
    const req = await api.get(`/clients/${id}`)
    return req.data
}

