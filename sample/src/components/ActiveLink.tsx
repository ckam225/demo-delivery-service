import { type } from 'os';
import * as React from 'react';
import { Link } from 'react-location';


type Props = {
    style?: object,
    className?:string
    activeClass?:string
    inactiveClass?:string
    to:string
}



const ActiveLink: React.FC<Props> = ({children, to, style, activeClass, className, ...rest}) => {
    function getActiveProps() {
        return {
          style,
          className: activeClass
        };
      }
    return <Link to={to} {...rest} className={className} getActiveProps={getActiveProps}>{children}</Link>
}


export default ActiveLink