import { FC } from "react";
import { Client } from "../../types/types";

type Props = {
    client: Client
}
const ClientTableItem:FC<Props> = ({client}) => {
    return <tr key={client.id}>
    <td>{client.id}</td>
    <td>{client.name}</td>
</tr>
}
 
export default ClientTableItem;