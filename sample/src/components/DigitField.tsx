
import React from 'react';

interface Props  extends React.HtmlHTMLAttributes<HTMLInputElement>{
    [x:string]:any
} 

const DigitField = ({ ...rest } : Props) => {
    return <input  {...rest} onInput={(e) => {
        e.currentTarget.value = e.currentTarget.value.replace(/[^0-9.]/g, '');
        e.currentTarget.value = e.currentTarget.value.replace(/(\..*)\./g, '$1');
    }} />
}

export default DigitField;