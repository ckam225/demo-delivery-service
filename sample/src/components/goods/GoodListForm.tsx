import React, { useEffect, useState } from "react";
import { Good } from "../../types/types";
import GoodForm, { initialGoodValue } from "./GoodForm";

type Props = {
    handleChange: Function
}
const GoodListForm:React.FC<Props> = ({handleChange}) => {

    const [values, setValues] = useState<Good[]>([{...initialGoodValue}])
   
    useEffect(() => handleChange(values), [values])

    function handleInputChange(id:string,e:React.ChangeEvent<HTMLInputElement>){
        const fieldValues = values.map(value => {
            const v = value as any
            if(value.id === id){
                v[e.target.name] = e.target.type === 'checkbox' ? e.target.checked :  e.target.value
            }
            return v
        })
        setValues(fieldValues)
    }
 
    function handleAddLine(index:number){
        setValues([...values, {...initialGoodValue, id: Date.now().toString()}])
    }

    function handleRemoveLine(index:number){
        const newValues = [...values]
        setValues(newValues.filter((v, i) =>  i !== index));
    }

    return <table>
        <thead>
            <tr>
                <th>Тип упаковки</th> 
                <th>Вес</th> 
                <th>Ширина</th>
                <th>Высота</th>
                <th>Длина</th> 
                <th>Объем</th>
                <th>Хрупкое</th>
                <th>Температурный режим</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            {values.map((good,i) => <GoodForm key={i} index={i} value={good} count={values.length}
                handleAddLine={handleAddLine}
                handleRemoveLine={handleRemoveLine}
                handleInputChange={handleInputChange}
            />)}
        </tbody>
    </table>
}
 
export default GoodListForm;