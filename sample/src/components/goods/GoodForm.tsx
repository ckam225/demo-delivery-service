import { type } from "os";
import React, { useEffect, useState } from "react";
import {VscAdd, VscChromeMinimize} from 'react-icons/vsc';
import { Good } from "../../types/types";
import DigitField from "../DigitField";

type Props = {
  index:number
   count:number
   inputClass?:string
   value:Good
   handleInputChange:Function
   handleAddLine:Function
   handleRemoveLine:Function
}

export const initialGoodValue:Good = {
    id: Date.now().toString(),
    package_type: '',
    weight: 0,
    width: 0,
    height: 0,
    length: 0,
    size: 0,
    fragile: 0,
    temp_conditions: '',
    cost: 0,
}

const GoodForm:React.FC<Props> = ({index=0, count=0, inputClass='', value=initialGoodValue, handleInputChange, handleAddLine, handleRemoveLine}) => {


    return  <tr>
        <td>
          <input type="text" name="package_type" className={["field", inputClass].join(' ')} placeholder="Тип упаковки" value={value.package_type} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="weight" className={["field", inputClass].join(' ')} placeholder="Вес" value={value.weight} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="width" className={["field", inputClass].join(' ')} placeholder="Ширина" value={value.width} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="height" className={["field", inputClass].join(' ')} placeholder="Высота" value={value.height} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="length" className={["field", inputClass].join(' ')} placeholder="Длина" value={value.length} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="size" className={["field", inputClass].join(' ')} placeholder="Объем" value={value.size} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <input type="checkbox" name="fragile" value={value.fragile} className={["field", inputClass].join(' ')}onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <input type="text" name="temp_conditions" className={["field", inputClass].join(' ')} placeholder="Температурный режим" value={value.temp_conditions} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        <td>
          <DigitField type="text" name="cost" className={["field", inputClass].join(' ')} placeholder="Стоимость" value={value.cost} onChange={(event) => handleInputChange(value.id, event)}/>
        </td>
        {count === 0 || index === count -1 && <td>
          <VscAdd className="cursor-pointer"  onClick={()=>handleAddLine(index)}/>
        </td>}
        {index != 0 && <td>
            <VscChromeMinimize className="cursor-pointer" onClick={()=>handleRemoveLine(index)}/>
        </td>}
        
    </tr>
}
 
export default GoodForm;