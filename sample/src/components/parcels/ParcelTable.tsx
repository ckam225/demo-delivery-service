import { FC } from "react";
import { Parcel } from "../../types/types";
import ParcelTableItem from "./ParcelTableItem";

type Props = {
    parcels: Parcel[],
    onItemClick: (p:Parcel) => void
}

const ParcelTable:FC<Props> = ({parcels =[], onItemClick}) => {
    return <table>
    <thead>
        <tr>
            {/* <th>Уникальный индентификатор</th> */}
            <th>Адрес отправления</th>
            <th>Адрес доставки</th>
            <th>Трек номер</th>
            <th>Стоимость доставки</th>
            <th>Фактическая дата забора</th>
            <th>Фактическая дата отправления</th>
            <th>Фактическая дата доставки</th>
            <th>Фактический срок доставки в днях</th>
            <th>Статус</th>
        </tr>
    </thead>
    <tbody>
        {parcels.length > 0 ? parcels.map(parcel => <ParcelTableItem onItemClick={(p) => onItemClick(p)} parcel={parcel} key={parcel.id} />) : <tr>
            <td colSpan={10} className="text-center">No parcels available</td>
        </tr>}
    </tbody>
</table>
}
 
export default ParcelTable;