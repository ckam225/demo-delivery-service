import * as React from 'react';
import { useAddress } from '../../hooks/useAddress';
import { Parcel } from "../../types/types";

type Props = {
    parcel: Parcel,
    onItemClick: (p:Parcel) => void
}
const ParcelTableItem:React.FC<Props> = ({parcel, onItemClick}) => {

    const {dispatchAddress, deliveryAddress} = useAddress(parcel)

    return <tr key={parcel.id} onClick={() => onItemClick(parcel)}>
    {/* <td>{parcel.id}</td> */}
    <td>{dispatchAddress}</td>
    <td>{deliveryAddress}</td>
    <td>{parcel.track_number}</td>
    <td>{parcel.delivery_cost}</td>
    <td>{parcel.fact_pickup_date}</td>
    <td>{parcel.fact_dispatch_date}</td>
    <td>{parcel.fact_delivery_date}</td>
    <td>{parcel.fact_delivery_days}</td>
    <td>{parcel.status}</td>
</tr>
}
 
export default ParcelTableItem;