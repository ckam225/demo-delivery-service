import * as React from 'react';
import { useAddress } from '../../hooks/useAddress';
import { ParcelPayload } from '../../types/payloads';
import { QuoteDeliveryCost, Request } from '../../types/types';
import GoodTable from '../goods/GoodTable';

const initialState = {
    track_number: '',
    status: 1
}
type Props = {
    quote: QuoteDeliveryCost,
    request: Request,
    submitting: boolean,
    onSubmit: (value:ParcelPayload) => void,
    onReset: () => void,
}


const ParcelForm:React.FC<Props> = ({quote, request, onSubmit, onReset}) => {

    const {dispatchAddress, deliveryAddress} = useAddress(request)
    const [parcel, setParcel] = React.useState<ParcelPayload>(initialState as ParcelPayload)

    React.useEffect(() => {
        setParcel({
            ...parcel,
            dispatch_address_id: request.dispatch_address?.id,
            delivery_address_id: request.delivery_address?.id,
            delivery_cost: quote.delivery_cost,
            fact_pickup_date: quote.fact_pickup_date,
            fact_dispatch_date: quote.fact_dispatch_date,
            fact_delivery_date: quote.fact_delivery_date,
            fact_delivery_days: quote.fact_delivery_days,
            goods: request.goods?.map((good => good.id || ''))
        })
    }, [quote, request])

    function handleChange(e:React.ChangeEvent<HTMLInputElement>){
        setParcel({
            ...parcel,
            track_number: e.target.value
        })
    }

    function handleSubmit(e: React.FormEvent){
        e.preventDefault()
        if(parcel.track_number){
            console.log('parcel', parcel);
            onSubmit(parcel)
            setParcel(initialState as ParcelPayload)
        }
    }

    function handleCancel(){
        setParcel(initialState as ParcelPayload)
        onReset()
    }

    return <form className="w-full" style={{minWidth: '600px'}} onSubmit={handleSubmit}>
        <div className="w-full flex justify-between">
            <div className="p-5">
                <div className="py-1">
                    <div className="text-gray-500">Клиент: </div> 
                    <em>{request.client?.name}</em>
                </div>
                <div className="py-1">
                    <div className="text-gray-500">Тип доставки: </div> 
                    <em>{request.delivery_type?.name}</em>
                </div>
            </div>
            <div className="p-5">
                <div className="py-1">
                    <div className="text-gray-500">Адрес отправления</div>
                    <div className=" text-sm">
                        {dispatchAddress}
                    </div>
                </div>
                <div className="py-1">
                    <div className="text-gray-500">Адрес получения</div>
                    <div className=" text-sm">
                        {deliveryAddress}
                    </div>
                </div>
            </div>
        </div>
        <div className=" w-full px-5">
            <div className="text-gray-500 text-center">Грузы</div> 
            {request?.goods && <GoodTable goods={request?.goods}/>}
        </div>
        <div className="px-5 my-2">
            <input type="text"
              placeholder='Трек номер'
              className='field' 
              name="track_number" 
              value={parcel.track_number} 
              onChange={handleChange}
            />
        </div>
        <div className=' p-5 flex justify-end'>
            <button className='btn btn-danger mr-2' type='button' onClick={handleCancel}>Отемить</button>
            <button className='btn' type='submit'>Сохрвнить</button>
        </div>
    </form>
}

export default ParcelForm