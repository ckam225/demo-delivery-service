import DeliverTableItem from "./DeliverGridItem";
import * as React from 'react';
import { Deliver } from "../../types/types";
import Modal, { ModalForwardProps } from "../Modal";

type Props  = {
    delivers: Deliver[],
    onItemClick: Function
}

const DeliverGird:React.FC<Props> = ({delivers =[], onItemClick}) => {

    function handleItemClick(deliver:Deliver){
        onItemClick(deliver)
    }

    return <div className="grid grid-cols-5 gap-2">
        {delivers.length > 0 ? delivers.map(deliver => <DeliverTableItem deliver={deliver} key={deliver.id} onClick={handleItemClick}/>) 
        : <div className="col-span-4">
           No delivers available
        </div>}
    </div>

   
}
 
export default DeliverGird;