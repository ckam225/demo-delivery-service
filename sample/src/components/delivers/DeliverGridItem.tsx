import * as React from 'react';
import { Deliver } from '../../types/types';
import icon from '../../assets/delivery-icon.svg'

type Props  = {
    deliver: Deliver,
    onClick: (deliver: Deliver) => void
}

const DeliverGridItem:React.FC<Props> = ({deliver, onClick}) => {
    return <div key={deliver.id} onClick={()=>onClick(deliver)} style={{cursor: 'pointer'}}
     className="border rounded-lg border-primary">
         <div className="flex justify-center">
             <img src={icon} width={64} />
         </div>
    <div className="p-2 text-center border-b">{deliver.name}</div>
</div>
}
 
export default DeliverGridItem;