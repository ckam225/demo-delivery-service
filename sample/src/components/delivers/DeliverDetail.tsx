import * as React from 'react';
import { Deliver } from '../../types/types';


type Props  = {
    deliver: Deliver
}

const DeliveryDetail: React.FC<Props> = ({deliver}) => {
    return <div>
        <p className="text-xs">{deliver.description}</p>
        <h1 className="text-sm text-center text-slate-500">Доступные типы доставки</h1>
        <div className="flex">
            {deliver.delivery_types.map(dt => <div key={dt.id} className="p-1 m-1 bg-primary text-white" style={{fontSize: '10px'}}>
            {dt.name}
            </div>)}
        </div>
        <h1 className="text-sm text-center text-slate-500">Вид транспортировки</h1>
        <div className="flex">
            {deliver.transport_types.map(dt => <div key={dt.id} className="p-1 m-1 bg-primary text-white" style={{fontSize: '10px'}}>
                {dt.name}
            </div>)}
        </div>
    </div>
}

export default DeliveryDetail