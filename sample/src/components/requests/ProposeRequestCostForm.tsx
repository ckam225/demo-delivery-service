import * as React from 'react';
import { fetchDelivers } from '../../api/delivers';
import { Deliver } from '../../types/types';
import DigitField from '../DigitField';
import dateFormat from "dateformat";

type Props = {
    handleSubmit: Function,
    isSubmitting: boolean
}

const ProposeRequestCostForm: React.FC<Props> = ({handleSubmit, isSubmitting=false}) => {

    const [delivers, setDelivers] =  React.useState<Deliver[]>([])
    const [formData, setFormData] = React.useState({
        deliver_id: '',
        delivery_cost: 0,
        fact_pickup_date: '',
        fact_dispatch_date: '',
        fact_delivery_date: '',
        fact_delivery_days: 1
    })
 
    React.useEffect(() => {
        setTimeout(async () => {
            const res = await fetchDelivers()
            setDelivers(res.data as Deliver[])
        }, 300)
    },[])

    function handleFieldChange(e:React.ChangeEvent<HTMLInputElement>){
        const value = {...formData} as any
        value[e.target.name] = e.target.value
        setFormData(value)
    }

    function handleSelectChange(e:React.ChangeEvent<HTMLSelectElement>){
        const value = {...formData} as any
        value[e.target.name] = e.target.value
        setFormData(value)
    }

    function getDate(date:string){
        return dateFormat(new Date(date), 'yyyy-mm-dd HH:MM:ss')
    }

    function handleFormSubmit(e:React.FormEvent){
        e.preventDefault()
        handleSubmit({
            ...formData,
            fact_pickup_date: getDate(formData.fact_pickup_date) ,
            fact_dispatch_date: getDate(formData.fact_dispatch_date) ,
            fact_delivery_date: getDate(formData.fact_delivery_date)
        })
    }

    return <form style={{width: '600px'}} className='p-5' onSubmit={handleFormSubmit}>

        <div className="w-full p-5 ">
            <div>Компания доставки</div>
            <select name="deliver_id" className="field" value={formData.deliver_id} onChange={handleSelectChange}>
                <option value=""></option>
                {delivers.map(dt => <option value={dt.id} key={dt.id}>{dt.name}</option>)}
            </select>
        </div>

        <div className="w-full p-5 ">
            <div>Фактическая дата забора</div>
            <input type="datetime-local" name="fact_pickup_date" className="field" value={formData.fact_pickup_date} onChange={handleFieldChange}/>
        </div>

        <div className="w-full p-5 ">
            <div>Фактическая дата отправления</div>
            <input type="datetime-local" name="fact_dispatch_date" className="field" value={formData.fact_dispatch_date} onChange={handleFieldChange}/>
        </div>

        <div className="w-full p-5 ">
            <div>Фактическая дата доставки</div>
            <input type="datetime-local" name="fact_delivery_date" className="field" value={formData.fact_delivery_date} onChange={handleFieldChange}/>
        </div>

        <div className="w-full p-5 ">
            <div>Фактический срок доставки в днях</div>
            <DigitField name="fact_delivery_days" className="field" value={formData.fact_delivery_days} onChange={handleFieldChange}/>
        </div>

        <div className="w-full p-5 ">
            <div>Стоимость доставки</div>
            <DigitField name="delivery_cost" className="field" value={formData.delivery_cost} onChange={handleFieldChange}/>
        </div>

        <div className='flex items-center justify-end'>
           {isSubmitting ? <span>loading...</span> : <button className="btn" type="submit">Предложить</button> }
        </div>
    </form>


}

export default ProposeRequestCostForm