
import * as React from 'react'
import { Link } from 'react-location'
import { Address, Request } from '../../types/types'

type Props = {
    request: Request
}

const RequestItem:React.FC<Props> = ({request}) => {

    const dispatchAddress = React.useMemo(() => {
        return formatAddress(request.dispatch_address)
    }, [])

    const deliveryAddress = React.useMemo(() => {
        return formatAddress(request.delivery_address)
    }, [])

    function formatAddress(address:Address){
        const l=Object.values(address);
        return l.slice(1, l.length - 1).join(', ')
    }

    return <tr>
        <td>
            <Link to={`/requests/${request.id}`} className='hover:text-primary-dark'>
               {request.id}
            </Link>
        </td>
        <td>{request.client.name}</td>
        <td title={request.delivery_type.description}>
            {request.delivery_type.name}
        </td>
        <td>{dispatchAddress}</td>
        <td>{deliveryAddress}</td>
    </tr>
}
 
export default RequestItem;