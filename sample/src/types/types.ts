
export  interface Address {
    id?: string
    country: string
    region: string
    city: string
    street: string
    house: string
    flat?: string
    index?: number
}

export  interface Client {
    id?:string
    name: string
}

export interface DeliveryType {
    id?: string;
    name: string;
    description: string;
}

export interface TransportType {
    id?: string;
    name: string;
    description: string;
}

export interface Good {
    id?: string;
    package_type: string;
    weight: number;
    width: number;
    height: number;
    length: number;
    size: number;
    fragile: number;
    temp_conditions?: any;
    cost: number;
    request_id?: string;
    parcel_id?: string;
}

export interface Deliver {
    id?: string;
    name: string;
    description?: string;
    delivery_types: DeliveryType[];
    transport_types: TransportType[];
}

export interface QuoteDeliveryCost {
    id: string;
    request_id: string;
    deliver: Deliver;
    delivery_cost: number;
    fact_pickup_date: string;
    fact_dispatch_date: string;
    fact_delivery_date: string;
    fact_delivery_days: number;
    created_at: Date;
    updated_at: Date;
}

export interface Request {
    id?: string;
    client: Client;
    dispatch_address: Address;
    delivery_address: Address;
    delivery_type: DeliveryType;
    goods: Good[];
    quote_delivery_costs: QuoteDeliveryCost[];
    created_at: Date;
}

export interface Parcel {
    id?: string;
    dispatch_address: Address;
    delivery_address: Address;
    track_number?: string;
    delivery_cost: number;
    fact_pickup_date?: string;
    fact_dispatch_date?: string;
    fact_delivery_date?: string;
    fact_delivery_days?: number;
    status: number|string;
    goods?: Good[];
}

export interface BaseHttpResponse{
    status: number
    error: string
}





