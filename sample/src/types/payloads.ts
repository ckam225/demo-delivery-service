


export type ParcelPayload = {
    dispatch_address_id?: string,
    delivery_address_id?: string,
    delivery_cost: number,
    fact_pickup_date: string,
    fact_dispatch_date: string,
    fact_delivery_date: string,
    fact_delivery_days: number,
    status: number,
    track_number:string,
    goods: string[]
}