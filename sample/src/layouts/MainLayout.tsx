
import { FC } from "react";
import {Link} from "react-location";
import ActiveLink from "../components/ActiveLink";



const MainLayout:FC = ({children}) => {
    return <div className="flex w-screen h-screen">
        <div className="sidebar h-full bg-blue-100 p-4">
            <ul>
                <li>
                    
                   <ActiveLink to="/clients"   className="nav-link" activeClass="text-primary-dark">
                      Клиенты
                   </ActiveLink>
                </li>
                <li>
                    <ActiveLink to="/delivers" className="nav-link" activeClass="text-primary-dark">
                       Компании доставки
                    </ActiveLink>
                </li>
                <li>
                    <ActiveLink to="/requests" className="nav-link" activeClass="text-primary-dark">
                       Запросы доставки
                    </ActiveLink>
                </li>
                <li>
                    <ActiveLink to="/parcels" className="nav-link" activeClass="text-primary-dark">
                      Посылки
                    </ActiveLink>
                </li>
                
            </ul>
        </div>
        <div className="flex flex-1">
            {children}
        </div>
    </div>
}
 
export default MainLayout;