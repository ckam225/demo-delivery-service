import React from 'react'
import ReactDom from "react-dom";

export const Head:React.FC = ({ children }) => {
    return ReactDom.createPortal(
        <>
            <meta charSet="utf-8" />
            <link rel="icon" type="image/svg+xml" href="/src/favicon.svg" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            
            {children}
        </>,
        document.querySelector("head") as HTMLHeadElement
    );
};