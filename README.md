
#### update submodules

```bash
git submodule update --init
```

#### Setup env variables and gitlab token
```
cp delivery-service/.env.example delivery-service/.env
ln -s delivery-service/.env .env
cp delivery-service/auth.json.example delivery-service/auth.json
sed -i "s/__token__/$GITLAB_AUTH_TOKEN/" delivery-service/auth.json
```

#### set api url 
```bash
echo "API_URL=..." >> .env
```

#### Build docker services

```bash
docker-compose build
```

#### Install dependencies
```bash
docker-compose run --rm api composer install
```

#### Start migrations and seed database
```
docker-compose run --rm api php artisan migrate --force
docker-compose run --rm api php artisan db:seed
```

#### Generate app key
```
docker-compose run --rm api php artisan key:generate
```


#### up docker services

```bash
docker-compose up -d
```

Visit http://[IP]:80